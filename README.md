# Shell Utilities

A collection of simple, non-production scripts, often to save remembering how
to do something.

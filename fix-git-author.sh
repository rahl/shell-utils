#!/bin/sh

# fix-git-author
# It is not unusual to have multiple git user accounts; e.g. personal, work,
# nefarious alter-ego name Neo.
# 
# Using them while logged in as one user - which has it's own global git
# config likely already set up (at least it should have) - can be tricky.
# 
# Occasionally we end up in a situation where a repository has been init'd
# or had one or more commits made using the wrong user details.
# 
# This script facilitates the process of recovering from such a situation.

#set -x

usage() {
    echo "Usage: $(basename $0) desired_name desired_email"
}

main() {
    if [ "$#" -ne 2 ]; then
        usage
        exit 1
    fi

    # REF: https://stackoverflow.com/a/1320137

    local desired_name=$1
    local desired_email=$2

    # Verification
    {
        local config_name=$(git config user.name)
        local config_email=$(git config user.email)
        if [ "$desired_name" != "$config_name" ]; then
            echo_mismatch $desired_name $config_name "name"
            exit 2
        fi
        if [ "$desired_email" != "$config_email" ]; then
            echo_mismatch $desired_email $config_email "email"
            exit 2
        fi
    }

    echo "== RESET ALL COMMITS =="
    echo "Name:  $desired_name"
    echo "Email: $desired_email"
    echo -e "\nWARNING! This will rebase and alter all your commit SHA's"
    echo -e "even if you technically don't need to run this."
    echo -en "Are you sure you wish to continue? [Yn] "

    read check

    if [ "$check" = "n" ] || [ "$check" = "N" ]; then
        echo "Aborting."
        exit 3
    fi

    echo "Resetting..."
    git rebase -r --root --exec "git commit --amend --no-edit --reset-author"

    # What follows is another way; not sure if it is "better" or "worse"
    # REF: https://stackoverflow.com/a/750182

    #git filter-branch --env-filter '
    #if [ "$GIT_COMMITTER_EMAIL" = "$old_email" ]
    #then
    #    export GIT_COMMITTER_NAME="$desired_name"
    #    export GIT_COMMITTER_EMAIL="$desired_email"
    #fi
    #if [ "$GIT_AUTHOR_EMAIL" = "$old_email" ]
    #then
    #    export GIT_AUTHOR_NAME="$desired_name"
    #    export GIT_AUTHOR_EMAIL="$desired_email"
    #fi
    #' --tag-name-filter cat -- --branches --tags
    
    # The following section comes from a response to the original referenced
    # stackoverflow comment.
    #git update-ref -d refs/original/refs/heads/master
    #rm -rf .git/refs/original
    #git log --pretty=format:"[%h] %cd - Committer: %cn (%ce), Author: %an (%ae)"
    #
}

echo_mismatch() {
    echo_error "Your desired $3 '$1' does not match the"  \
        "config for this git directory [$2].\n"                             \
        "Please do e.g \`git config user.$3 $1\` first."
}

echo_error() {
    echo -en "[ERROR]: $@\n"
}

main "$@"
